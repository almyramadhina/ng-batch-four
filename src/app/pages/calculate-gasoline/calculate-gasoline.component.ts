// import { Component } from '@angular/core';

// @Component({
//   selector: 'app-calculate-gasoline',
//   templateUrl: './calculate-gasoline.component.html',
//   styleUrls: ['./calculate-gasoline.component.scss']
// })

// export class CalculateGasolineComponent {
//   liter = 0
//   hargaBensin = 0
//   bensin = ""
//   jumlahBensin = 0
//   totalBayar = 0
//   totalHarga = 0
//   kembali = 0

//   testClass = 'alert alert-success'

//   constructor() {
//     this.hargaBensin = this.dataService.baseHarga
//   }

//   hitungHarga(jumlahBensin: any, hargaBensin: any) {
//     // this.totalBayar = jumlahBensin*hargaBensin
//     this.totalHarga = jumlahBensin*hargaBensin
//     this.kembali = this.totalBayar - this.totalHarga
//   }

//   doClickBensin(inputBensin: any) {

//     this.bensin = inputBensin

//     if (inputBensin == "pertamax") {
//       this.hargaBensin = 10000
//     } else if (inputBensin == "pertalite") {
//       this.hargaBensin = 20000
//     } else if (inputBensin == "solar") {
//       this.hargaBensin = 30000
//     } else if (inputBensin == "pertamax plus") {
//       this.hargaBensin = 40000
//     }
//   }
// }

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-calculate-gasoline',
  templateUrl: './calculate-gasoline.component.html',
  styleUrls: ['./calculate-gasoline.component.scss'],
})
export class CalculateGasolineComponent {
  testClass = 'alert alert-success';

  constructor(private dataService: DataService) {
    this.harga = this.dataService.baseHarga;
  }

  @Input() jumlahLiter = 0;
  @Input() jumlahBayar = 0;

  @Output() dataCallBack = new EventEmitter();

  kembalian = 0;
  name = '';
  harga = 0;

  doClick(bensin: any) {
    this.name = bensin;
    if (bensin == 'Pertamax') {
      this.harga = 10000;
      this.name = 'Pertamax';
    } else if (bensin == 'Pertalite') {
      this.harga = 12000;
      this.name = 'Pertalite';
    } else if (bensin == 'Solar') {
      this.harga = 16000;
      this.name = 'Solar';
    } else {
      this.harga = 20000;
      this.name = 'Pertamax Plus';
    }
  }

  doHitung() {
    if (this.harga === 10000) {
      this.testClass = 'alert alert-success';
    } else {
      this.testClass = 'alert alert-danger';
    }
    
    if (this.name == 'Pertamax') {
      this.kembalian = this.jumlahBayar - this.jumlahLiter * this.harga;
    } else if (this.name == 'Pertalite') {
      this.kembalian = this.jumlahBayar - this.jumlahLiter * this.harga;
    } else if (this.name == 'Solar') {
      this.kembalian = this.jumlahBayar - this.jumlahLiter * this.harga;
    } else {
      this.kembalian = this.jumlahBayar - this.jumlahLiter * this.harga;
    }
  }
}