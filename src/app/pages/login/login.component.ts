import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Route, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent {
  username = ''
  password = ''

  formLogin: FormGroup

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.formLogin = this.formBuilder.group({
      // username: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    })
  }

  get errorControl() {
    return this.formLogin.controls;
  }
  
  doLogin() {

    console.log(this.formLogin)

    const payload = {
      username: this.formLogin.value.username,
      password: this.formLogin.value.password
    }

    this.authService.login(payload).subscribe(
      response => {
        console.log(response)
        // if(response.token) {
        //   this.router.navigate(['/playground'])
        // }

        // Swal.fire({
        //   title: 'Success!',
        //   text: 'You are logged in as admin',
        //   icon: 'success',
        //   confirmButtonText: 'Ok'
        // })

        // alert(response.data.jwtrole)

        if(response.data.jwtrole === 'admin') {
          localStorage.setItem('token', response.data.token)
          Swal.fire({
            title: 'Success!',
            text: 'You are logged in as admin',
            icon: 'success',
            confirmButtonText: 'Ok'
          })
          this.router.navigate(['/admin/dashboard'])
        } else if(response.data.jwtrole === 'user') {
          this.router.navigate(['/users'])
          Swal.fire({
            title: 'Not allowed!',
            text: 'You are not authenticated as admin',
            icon: 'error',
            confirmButtonText: 'Ok'
          })
        }
      }, error => {
        console.log(error)
        // alert(error.error.message)

        Swal.fire({
          title: 'Failed!',
          text: 'Username or password is unmatched',
          icon: 'error',
          confirmButtonText: 'Try again'
        })
      }
      
      
    )
  }
}
