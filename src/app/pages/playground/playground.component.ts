import { Component } from '@angular/core';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})
export class PlaygroundComponent {
  title = 'ng-batch-four';

  name = "scoups"
  age = 26
  status = false

  person = {
    title: 'Test A',
    name: 'jeonghan',
    age: 26,
    status: true
  }

  personList = [
    {
      title: 'Test A',
      name: 'jeonghan',
      age: 26,
      status: true,
    },
    {
      title: 'Test B',
      name: 'joshua',
      age: 27,
      status: false,
    }
  ]

  // datas = [1,2,3]
  // datas = new Array(3)

  showData = false

  nomor = 5

  constructor (){
    this.name = "hosh"
    this.age = 25
  }

  onCallBack(ev: any) {
    console.log(ev);
  }
}