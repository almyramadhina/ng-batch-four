import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { RouterModule, Routes } from '@angular/router';
import { LayoutsModule } from 'src/app/layouts/layouts.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SettingsComponent } from './settings/settings.component';
import { ListTicketComponent } from './product/list-ticket/list-ticket.component';
import { ListUserComponent } from './product/list-user/list-user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListEventComponent } from './product/list-event/list-event.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'settings',
        component: SettingsComponent
      },
      {
        path: 'list-user',
        component: ListUserComponent
      },
      {
        path: 'list-ticket',
        component: ListTicketComponent
      },
      {
        path: 'list-event',
        component: ListEventComponent
      }
    ]
  }
]

@NgModule({
  declarations: [
    AdminComponent,
    DashboardComponent,
    SettingsComponent,
    ListTicketComponent,
    ListUserComponent,
    ListEventComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    LayoutsModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class AdminModule { }
