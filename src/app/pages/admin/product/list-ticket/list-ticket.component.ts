
import { FormBuilder, FormGroup } from '@angular/forms';
import { Ticket } from 'src/app/interfaces/ticket.interface';
import { TicketsService } from 'src/app/services/tickets.service';
import { Component, Input, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { formatCurrency } from '@angular/common';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-ticket', 
  templateUrl: './list-ticket.component.html',
  styleUrls: ['./list-ticket.component.scss']
})
export class ListTicketComponent {
  tickets: Ticket[] = []
  modalRef?: BsModalRef;
  updateForm: FormGroup
  @Input() idEvent = 0;
  @Input() section = '';
  @Input() price = 0;
  @Input() stock = 0;
  @Input() forMember = false;

  ticketDetail!: Ticket
  constructor(private ticketsService: TicketsService, private modalService: BsModalService, private Sb: FormBuilder) {
    this.ticketsService.getTicketList().subscribe(
      response => {
        console.log(response)
        this.tickets = response
      }, error => {
        console.log(error)
        alert(error.error.message)
      }
    )

    this.updateForm = this.Sb.group({
      id: [''],
      idEvent: [''],
      section: [''],
      price: [''],
      stock: [''],
      forMember: ['']
    })
  }
 
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    // this.ticketDetail = data
  }

  updateModal(data: Ticket) {
    console.log(data)
    this.ticketDetail = data 

    this.updateForm.controls['section'].patchValue(data.section)
    this.updateForm.controls['price'].patchValue(data.price)
    this.updateForm.controls['stock'].patchValue(data.stock)
    this.updateForm.controls['forMember'].patchValue(data.forMember)
  }


  doAddTicket() {
    const payload = {
      idEvent: this.idEvent,
      section: this.section,
      price: this.price,
      stock: this.stock,
      forMember: this.forMember
    }

    this.ticketsService.addTicket(payload).subscribe(
      res => {
        console.log(res)

        Swal.fire({
          title: 'Success!',
          text: 'Ticket has been added',
          icon: 'success',
          confirmButtonText: 'Ok'
        })
      }, error => {
        console.log(error)
        alert(error.error.message)

        Swal.fire({
          title: 'Failed!',
          text: 'Ticket cant be added',
          icon: 'error',
          confirmButtonText: 'Ok'
        })
      } 
    )
  }

  doUpdateTicket() {
    const payload = {
      section: this.section,
      price: this.price,
      stock: this.stock,
      forMember: this.forMember
    }

    this.ticketsService.updateTicket(payload, this.ticketDetail.idTicket).subscribe(
      res => {
        console.log(res)

        Swal.fire({
          title: 'Success!',
          text: 'Ticket has been updated',
          icon: 'success',
          confirmButtonText: 'Ok'
        })
      }, error => {
        console.log(error)
        alert(error.error.message)

        Swal.fire({
          title: 'Failed!',
          text: 'Ticket cant be updated',
          icon: 'error',
          confirmButtonText: 'Ok'
        })
      } 
    )
  }
}
