import { Component, Input, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { EventsService } from 'src/app/services/events.service';
import { Events } from 'src/app/interfaces/event.interface';
import Swal from 'sweetalert2';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { map, Subject, switchMap, takeUntil } from 'rxjs';

@Component({
  selector: 'app-list-event',
  templateUrl: './list-event.component.html',
  styleUrls: ['./list-event.component.scss']
})
export class ListEventComponent {
  event!: Events[];
  formAdd: FormGroup;
  formEdit: FormGroup;
  modalRef?: BsModalRef;

  image!: string;
  imageFile!: File;

  refresh = new Subject<void>();

  eventDetail: Events = {
    idEvent: 0,
    artistName: '',
    eventName: '',
    eventDate: '',
    eventTime: '',
    promotor: '',
    venue: '',
    poster: ''
  };

  constructor(
    private eventServices: EventsService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder
  ) {
    this.loadData();
    this.formAdd = this.formBuilder.group({
      artistName: ['', [Validators.required]],
      eventName: ['', [Validators.required]],
      eventDate: ['', [Validators.required]],
      eventTime: ['', [Validators.required]],
      promotor: ['', [Validators.required]],
      venue: ['', [Validators.required]],
      poster: ['', [Validators.required]],
    });
    this.formEdit = this.formBuilder.group({
      artistName: [''],
      eventName: [''],
      eventDate: [''],
      eventTime: [''],
      promotor: [''],
      venue: [''],
      poster: ['']
    });
  }

  loadData() {
    this.eventServices
      .getEventList()
      .pipe(takeUntil(this.refresh))
      .subscribe((response) => {
        console.log(response);
        this.event = response;
      });
  }

  get errorControl() {
    return this.formAdd.controls;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  showPreview(event: any) {
    if (event) {
      const file = event.target.files[0];
      this.imageFile = file;
      const reader = new FileReader();
      reader.onload = () => {
        this.image = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }

  doAddEvent() {
    if (this.imageFile) {
      Swal.fire({
        title: 'Success!',
        text: 'You have successfully add 1 event',
        icon: 'success',
        confirmButtonColor: '#0046e6',
        confirmButtonText: 'OK',
      });
      this.eventServices
        .uploadPhoto(this.imageFile)
        .pipe(
          switchMap((val) => {
            const payload = {
              artistName: this.formAdd.value.artistName,
              eventName: this.formAdd.value.eventName,
              eventDate: this.formAdd.value.eventDate,
              eventTime: this.formAdd.value.eventTime,
              promotor: this.formAdd.value.promotor,
              venue: this.formAdd.value.venue,
              image: this.formAdd.value.image,
              poster: val.data,

            };
            // this.imageFile = null;
            return this.eventServices
              .addEvent(payload)
              .pipe(map((val) => val));
          })
        )
        .subscribe((response) => {
          console.log(response);
          this.loadData();
        });
    }
  }

  doDeleteEvent() {
    if (this.imageFile) {
      Swal.fire({
        title: 'Event has been deleted!',
        text: 'You have successfully delete 1 event',
        icon: 'success',
        confirmButtonColor: '#0046e6',
        confirmButtonText: 'OK',
      });

      this.eventServices
        .uploadPhoto(this.imageFile)
        .pipe(
          switchMap((val) => {
            const payload = {
              artistName: this.formAdd.value.artistName,
              eventName: this.formAdd.value.eventName,
              eventDate: this.formAdd.value.eventDate,
              eventTime: this.formAdd.value.eventTime,
              promotor: this.formAdd.value.promotor,
              venue: this.formAdd.value.venue,
              image: this.formAdd.value.image,
              poster: val.data,
            };
            // this.imageFile = null;
            return this.eventServices
              .deleteEvent(this.eventDetail.idEvent)
              .pipe(map((val) => val));
          })
        )
        .subscribe((response) => {
          console.log(response);
          this.loadData();
        });
    } else {
      Swal.fire({
        title: 'Event has been deleted!',
        text: 'You have successfully delete 1 event and ticket in this event',
        icon: 'success',
        confirmButtonColor: '#0046e6',
        confirmButtonText: 'OK',
      });
      // const payload = {
      //   artistName: this.formAdd.value.artistName,
      //   eventName: this.formAdd.value.eventName,
      //   eventDate: this.formAdd.value.eventDate,
      //   eventTime: this.formAdd.value.eventTime,
      //   promotor: this.formAdd.value.promotor,
      //   venue: this.formAdd.value.venue,
      //   image: this.formAdd.value.image,
      //   poster: val.data,
      // };
      this.eventServices
        .deleteEvent(this.eventDetail.idEvent)
        .pipe(map((val) => val))

        .subscribe((response) => {
          console.log(response);
          this.loadData();
        });
    }
  }

  doUpdateEvent() {
    if (this.imageFile) {
      Swal.fire({
        title: 'Success',
        text: 'Event has been updated!',
        icon: 'success',
        confirmButtonColor: '#0046e6',
        confirmButtonText: 'OK',
      });

      this.eventServices
        .uploadPhoto(this.imageFile)
        .pipe(
          switchMap((val) => {
            const payload = {
              artistName: this.formEdit.value.artistName,
              eventName: this.formEdit.value.eventName,
              eventDate: this.formEdit.value.eventDate,
              eventTime: this.formEdit.value.eventTime,
              promotor: this.formEdit.value.promotor,
              venue: this.formEdit.value.venue,
              image: this.formEdit.value.image,
              poster: val.data,
            };
            // this.imageFile = null;
            return this.eventServices
              .updateEvent(payload, this.eventDetail.idEvent)
              .pipe(map((val) => val));
          })
        )
        .subscribe((response) => {
          console.log(response);
          this.loadData();
        });
    } else {
      Swal.fire({
        title: 'Success',
        text: 'Event has been updated!',
        icon: 'success',
        confirmButtonColor: '#0046e6',
        confirmButtonText: 'OK',
      });
      const payload = {
        artistName: this.formEdit.value.artistName,
        eventName: this.formEdit.value.eventName,
        eventDate: this.formEdit.value.eventDate,
        eventTime: this.formEdit.value.eventTime,
        promotor: this.formEdit.value.promotor,
        venue: this.formEdit.value.venue,
        poster: this.eventDetail.poster
      };
      this.eventServices
        .updateEvent(payload, this.eventDetail.idEvent)
        .pipe(map((val) => val))

        .subscribe((response) => {
          console.log(response);
          this.loadData();
        });
    }
  }

  doUpdateModal(data: Events) {
    console.log(data);
    this.eventDetail = data;
    this.formEdit.controls['artistName'].patchValue(data.artistName);
    this.formEdit.controls['eventName'].patchValue(data.eventName);
    this.formEdit.controls['eventDate'].patchValue(data.eventDate);
    this.formEdit.controls['eventTime'].patchValue(data.eventTime);
    this.formEdit.controls['promotor'].patchValue(data.promotor);
    this.formEdit.controls['venue'].patchValue(data.venue);
    this.formEdit.controls['poster'].patchValue(data.poster);
  }


  doDeleteModal(data: Events) {
    console.log(data);
    this.eventDetail = data;
  }


}
