import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  username = ''
  password = ''
  role = ''
  email = ''
  fullname = ''
  formRegister: FormGroup

  constructor(private authService: AuthService, private router: Router, private formBuilder: FormBuilder) {
    this.formRegister = this.formBuilder.group({
      // username: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required,Validators.minLength(10), Validators.maxLength(13)]],
      password: ['', [Validators.required]],
      role: ['', [Validators.required]],
      email: ['', [Validators.required]],
      fullname: ['', [Validators.required]]
    })
  }

  get errorControl() {
    return this.formRegister.controls;
  }
  
  doRegister() {
    const payload = {
      username: this.username,
      password: this.password,
      role: this.role,
      email: this.email,
      fullname: this.fullname
    }

    this.authService.register(payload).subscribe(
      response => {
        console.log(response)
      }
    )
  }
}
