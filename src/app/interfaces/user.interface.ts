export interface User {
  id: number,
  username: string,
  email: string,
  nik: string,
  birthDate: Date,
  fullName: string,
  vaccineCertificate: string,
  member: boolean,
  admin: boolean
}

export interface ResponseUploadPhoto {
  image: string,
}