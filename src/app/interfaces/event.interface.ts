export interface Events {
    idEvent: number,
    artistName: string,
    eventName: string,
    eventDate: any,
    eventTime: any,
    promotor: string,
    venue: string,
    poster: string
  }

export interface ResponseUploadPhoto {
  image: string;
}