import { Events } from "./event.interface";

export interface Ticket {
  idTicket: number;
  section: string;
  price: number;
  stock: number;
  event: Events;
  forMember: boolean;
}
