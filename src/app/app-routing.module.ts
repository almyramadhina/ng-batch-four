import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './components/admin/admin.component';
import { AdminGuard } from './guards/admin.guard';
import { UserGuard } from './guards/user.guard';
import { DashboardComponent } from './pages/admin/dashboard/dashboard.component';
import { ListTicketComponent } from './pages/admin/product/list-ticket/list-ticket.component';
import { ListUserComponent } from './pages/admin/product/list-user/list-user.component';
import { CalculateGasolineComponent } from './pages/calculate-gasoline/calculate-gasoline.component';
import { LoginComponent } from './pages/login/login.component';
import { PlaygroundComponent } from './pages/playground/playground.component';
import { RegisterComponent } from './pages/register/register.component';
import { UsersComponent } from './pages/users/users.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'playground',
    component: PlaygroundComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'calculate',
    component: CalculateGasolineComponent
  },
  {
    path: 'users',
    component: UsersComponent
  },
  {
    path: 'admin',
    loadChildren: () => import('./pages/admin/admin.module').then( m => m.AdminModule),
    canLoad: [AdminGuard]
  },
  {
    path: 'user',
    component: UsersComponent,
    canActivate: [UserGuard]
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
