import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {
  @Input() title = 'A'

  @Input() name = ''
  @Input() age = 0
  @Input() status = false

  @Output() dataCallBack = new EventEmitter()

  doClick() {
    this.dataCallBack.emit({ data: { name: this.name, age: this.age, status: this.status }})
    
    // this.dataCallBack.emit({ data: { status: this.status }})
    // this.dataCallBack.emit({ data: { age: this.age }})
  }
}