import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, map, catchError } from 'rxjs';
import { ResponseBase } from '../interfaces/response.interface';
import { ResponseUploadPhoto, User } from '../interfaces/user.interface';
import { Ticket } from '../interfaces/ticket.interface';

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  private baseApi = 'http://localhost:8080'

  constructor(private httpClient: HttpClient) { }

  getTicketList(): Observable<Ticket[]> {
    return this.httpClient
      .get<ResponseBase<Ticket[]>>(`${this.baseApi}/ticket/all`)
      .pipe(
        map( val => {
          return val.data
        }),
        catchError((err) => {
          console.log(err)
          throw err
        })
      )
  }

  addTicket(payload: {
    idEvent: number,
    section: string,
    price: number,
    stock: number,
    forMember: boolean
}): Observable<Ticket[]> {
    return this.httpClient
      .post<ResponseBase<Ticket[]>>(`${this.baseApi}/ticket/add`, payload)
      .pipe(
        map( val => {
          return val.data
        }),
        catchError((err) => {
          console.log(err)
          throw err
        })
      )
  }

  updateTicket(payload: {
    section: string,
    price: number,
    stock: number,
    forMember: boolean
  }, id: number): Observable<Ticket[]> {
    return this.httpClient
      .post<ResponseBase<Ticket[]>>(this.baseApi + '/ticket/update/' + id, payload)
      .pipe(
        map( val => {
          return val.data
        }),
        catchError((err) => {
          console.log(err)
          throw err
        })
      )
  }

  // editTicket(payload)
}
