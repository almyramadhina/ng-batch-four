import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, map, catchError } from 'rxjs';
import { ResponseBase } from '../interfaces/response.interface';
import { ResponseUploadPhoto, User } from '../interfaces/user.interface';
import { Events } from '../interfaces/event.interface';
import { Time } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  private baseApi = 'http://localhost:8080'

  constructor(private httpClient: HttpClient) { }

  getEventList(): Observable<Events[]> {
    return this.httpClient
      .get<ResponseBase<Events[]>>(`${this.baseApi}/event/all`)
      .pipe(
        map((val) => {
          const data: Events[] = [];
          // mapping data for new response
          for (let item of val.data) {
            data.push({
              idEvent: item.idEvent,
              artistName: item.artistName,
              eventName: item.eventName,
              eventDate: item.eventDate,
              eventTime: item.eventTime,
              promotor: item.promotor,
              venue: item.venue,
              poster: `${this.baseApi}/event/files/${item.poster}:.+`,
            });
          }
          return data;
        }),
        catchError((error) => {
          console.log(error);
          throw error;
        })
      );
  }

//   addEvent(payload: {
//     artistName: string,
//     eventName: string,
//     eventDate: Date,
//     eventTime: Time,
//     promotor: string,
//     venue: string,
//     poster?: string
// }): Observable<Event[]> {
//     return this.httpClient
//       .post<ResponseBase<Event[]>>(`${this.baseApi}/event/add`, payload)
//       .pipe(
//         map( val => {
//           return val.data
//         }),
//         catchError((err) => {
//           console.log(err)
//           throw err
//         })
//       )
//   }

// addEvent(payload: {
//   artistName: string,
//   eventName: string,
//   eventDate: Date,
//   eventTime: Time,
//   promotor: string,
//   venue: string,
//   poster?: string
// }) {
//   return this.httpClient.post(this.baseApi + '/event/add', payload);
// }

  // addEvent(payload: any) {
  //   return this.httpClient.post<ResponseBase<any>>(
  //     `${this.baseApi}/event/add`,
  //     payload
  //   );
  // }
  
  addEvent(payload: {
    artistName: string,
    eventName: string,
    eventDate: Date,
    eventTime: Time,
    promotor: string,
    venue: string,
    poster?: string
  }) {
    return this.httpClient.post(this.baseApi + '/event/add', payload);
  }

  uploadPhoto(data: File): Observable<ResponseBase<string>> {
    // const token = localStorage.getItem('token');
    // const headers = new HttpHeaders({
    //   Authorization: `Bearer ${token}`,
    // });
    const file = new FormData();
    file.append('file', data, data.name);
    return this.httpClient.post<ResponseBase<string>>(
      `${this.baseApi}/event/upload-file`,
      file
    );
  }

  updateEvent(payload: {
    artistName: string,
    eventName: string,
    eventDate: any,
    eventTime: any,
    promotor: string,
    venue: string,
    poster: string
  }, idEvent: number): Observable<Events[]> {
    return this.httpClient
      .post<ResponseBase<Events[]>>(this.baseApi + '/event/update/' + idEvent, payload)
      .pipe(
        map( val => {
          return val.data
        }),
        catchError((err) => {
          console.log(err)
          throw err
        })
      )
  }

  deleteEvent(
      idEvent: number
  ) {
    return this.httpClient.delete(this.baseApi + '/event/delete/' + idEvent);
  }

}
