import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, map, catchError } from 'rxjs';
import { ResponseBase } from '../interfaces/response.interface';
import { ResponseUploadPhoto, User } from '../interfaces/user.interface';
import { Ticket } from '../interfaces/ticket.interface';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private baseApi = 'http://localhost:8080'

  constructor(private httpClient: HttpClient) { }

  getUserList(): Observable<User[]> {

    // const token = localStorage.getItem('token')

    // const header = new HttpHeaders({
    //   'Authorization': `Bearer ${token}`
    // })
    
    return this.httpClient
      .get<ResponseBase<User[]>>(`${this.baseApi}/user/all`)
      .pipe(
        map( val => {
          return val.data
        }),
        catchError((err) => {
          console.log(err)
          throw err
        })
      )
  }


  uploadPhoto(data:File): Observable<ResponseBase<ResponseUploadPhoto>> {
    const file = new FormData()
    file.append('file', data, data.name)
    return this.httpClient.post<ResponseBase<ResponseUploadPhoto>>(`${this.baseApi}/users/uploadPhoto`, file)
  }
}
